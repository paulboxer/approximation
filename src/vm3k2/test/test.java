package vm3k2.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.File;

import Jama.Matrix;
import vm3k2.*;

public class test extends Kernel{	
	test(){
		super();
		try {
		debuglog = new PrintWriter(new File("5_debuglog_CTest.txt").getAbsoluteFile());
		 } catch(IOException e) {throw new RuntimeException(e);}
	}
	
		
	@Override	//��������������� ��������������� ������, ������ �������� � ����������� ������� - ������
	public void writebegindatafile(PrintWriter file){
		file.println("������������ ��������������� ��������������� ������");//��������� �����
	}
	
	public void test_working_jama(){
		
		double[][] array_2 = {	{11.0,	-3.0},
							 	{-15.,	-2.0}   };
		Matrix A = new Matrix(array_2);
		print(A.det());	//-67
		
		double[][] array_3 = {	{1.0,	-2.,	3.0},
								{4.0,	0.0,	6.0},
								{-7.,	8.0,	9.0}};
		A = new Matrix(array_3);
		print(A.det()); //204
		
		double[][] array_4 = {	{3.0,	-3.,	-5.,	8.0},
								{-3.,	2.0,	4.0,	-6.},
								{2.0,	-5.,	-7.,	5.0},
								{-4.,	3.,	    5.0,	-6.}	};
				
		print((new Matrix(array_4)).det()); //18	
		
		//Double onedimarr[] = {3.0,	-3.,	-5.,	8.0};		
		//Matrix onedimM = {3.0,	-3.,	-5.,	8.0};		
		//Matrix onedimM = new Matrix(onedimarr, 4);		
		//double arr[] = onedimM.getColumnPackedCopy();//.getArray()		
		//double arr2[] = new Matrix(onedimarr, onedimarr.length).getColumnPackedCopy();
		
		print("");
		
		
		
	}
	
	String[] GetListFileInFolder() {
		String path = System.getProperty("user.dir") + "InFolder";
		File f = new File(path);
        String[] list = f.list();     //������ ������ � ������� �����
        return list;
	}

	
	
	public static void main (String[] args) throws java.lang.Exception
	{
		new test().GeneralProcessing();		
	}
	
	
	
	public void GeneralProcessing(){
		print("Begin class test");		
		writebegindatafile(debuglog);		
		test_working_jama();		
		close();		
	}
	

}


