package vm3k2;

// ����������� �������:
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.math.BigDecimal;
import java.math.RoundingMode;

// ����� ������:
class PlotFrame extends Frame {	
	
	public static void main(String args[]){
		System.out.println("HelloWindow");
		// �������� ����:
		//new PlotFrame(400+250,500);
	}
	
	// ����������� (��������� - ������ � ������ ����):
	PlotFrame(int H,int W, PlotData PD){
			
		// ��������� ����:
		setTitle("������ �������");
		setBounds(100,50,W,H);        // ��������� � ������ ����
		setBackground(Color.GRAY);    // ���� ���� ����
		setLayout(null);        // ���������� ��������� ���������� ���������
		Font f=new Font("Arial",Font.BOLD,11);  // ����������� ������
		setFont(f);                             // ���������� ������
			
		BPanel BPnl=new BPanel(6,25,W/4,H-30, PD.lFold);   // �������� ������ � ��������
		add(BPnl);                               // ���������� ������ � ������� ����
		
		//�������
			// ������ ��� ����������� ������� (��������):
			PPanel PPnl_1 = new PPanel(W/4+10,60,3*W/4-15,H/3,BPnl, PD.x, PD.y);			
			PPanel PPnl_2 = new PPanel(W/4+10,345,3*W/4-15,H/3,BPnl, PD.xpoly, PD.ypoly);
			// ���������� ������ � ������� ����:
			add(PPnl_1);
			add(PPnl_2);	
		
		//��������� ��������
			LPanel LPnl_1=new LPanel(W/4+10,35,3*W/4-15,20, "�������� ������");
			//���������� ������ � ������� ����:
			add(LPnl_1);	
			
			LPanel LPnl_2=new LPanel(W/4+10,320,3*W/4-15,20, "����������������� ������ � ����� 0.1");
			// ���������� ������ � ������� ����:
			add(LPnl_2);
			
			HPanel HPnl=new HPanel(W/4+10,H-90-25,3*W/4-15,85+20);
			// ���������� ������ � ������� ����:
			add(HPnl);
			
				
		// ����������� ����������� � ���� (�������� ����):
		addWindowListener(new WindowAdapter(){ public void windowClosing(WindowEvent ve)  {System.exit(0);}  /*��������*/} );
			
			// ����������� ����������� ��� ������ ������ ������� �������
			BPnl.B1.addActionListener(new Button1Pressed(BPnl,PPnl_1, PD, true));
			
			// ����������� ����������� ��� ������ ������ ������� �������
			BPnl.B1.addActionListener(new Button1Pressed(BPnl,PPnl_2, PD, false));			
			
			
			// ����������� ����������� ��� ������ ������:
			BPnl.B2.addActionListener(new Button2Pressed());
			// ����������� ����������� ��� ������ ������ �����:
			BPnl.Cb.addItemListener(new cbChanged(BPnl));
			
			// ������ ���� (������) �� ����������:
			setResizable(false);
									
			setVisible(true);              // ����������� ����
		}
}
		
// ����� ������ � ��������:
class BPanel extends Panel{
	
	// ����� ������:
	public Label L[] = new Label[4];
	// ������������� ������:
	public Checkbox Cb;
	// �������������� ������:
	public Choice Ch[];
	// ������ ������:
	public Button B1,B2;

	// ����������� 
	// (��������� - ���������� � ������� ������):
	BPanel(int x,int y,int W,int H, String lFold[]){		
		//iFold - ������ ��������� � �������� ������� ������		
		// ���������� ��������� ���������� ��������� �� ������:
		setLayout(null);  
		
		setBounds(x,y,W,H);   // ��������� � ������ ������
		
		// ������ �����:
		L[0]=new Label();		
		// ��������� �����:
		L[0]=new Label("��������� ������",Label.CENTER);		
		// ����� ��� ��������� �����:
		L[0].setFont(new Font("Arial",Font.BOLD,12));		
		// ������� �����:
		L[0].setBounds(5,5,getWidth()-10,30);		
		// ���������� ����� �� ������:
		add(L[0]);
		
		// ������ �����:
		L[2]=new Label();		
		// ��������� �����:
		L[2]=new Label("����� ��������",Label.LEFT);		
		// ����� ��� ��������� �����:
		L[2].setFont(new Font("Arial",Font.BOLD,12));		
		// ������� �����:
		L[2].setBounds(5,90,getWidth()-10,30);		
		// ���������� ����� �� ������:
		add(L[2]);			
		
		// �������������� ������ 
		Ch = new Choice[2];		
		Ch[0] = new Choice();
		
		// ���������� ��������� 
		int i=0;
		while(i<lFold.length){			
			Ch[0].add(lFold[i]);		
			i++;
		}
		
		// ������ � ��������� ��������������� ������:
		Ch[0].setBounds(5,120,getWidth()-15,30);		
		
		// ���������� ������ �� ������:
		add(Ch[0]);	
		
		// ������ �����:
		L[3]=new Label();		
		// ��������� �����:
		L[3]=new Label("����� ������",Label.LEFT);		
		// ����� ��� ��������� �����:
		L[3].setFont(new Font("Arial",Font.BOLD,12));		
		// ������� �����:
		L[3].setBounds(5,150,getWidth()-10,30);		
		// ���������� ����� �� ������:
		add(L[3]);
		
		Ch[1] = new Choice();
		// ���������� �������� "�������":
		Ch[1].add("�������");		
		// ���������� �������� "������":
		Ch[1].add("������");		
		// ���������� �������� "��������":
		Ch[1].add("��������");		
		// ������ � ��������� ��������������� ������:
		Ch[1].setBounds(5,180,getWidth()-15,30);		
		// ���������� ������ �� ������:
		add(Ch[1]);
		Ch[1].setEnabled(false);		
		
		// ������ �����:
		L[1]=new Label();		
		// ��������� �����:
		L[1]=new Label("����������� �����",Label.LEFT);		
		// ����� ��� ��������� �����:
		L[1].setFont(new Font("Arial",Font.BOLD,12));		
		// ������� �����:
		L[1].setBounds(5,240,getWidth()-10,30);		
		// ���������� ����� �� ������:
		add(L[1]);		
		
		Cb=new Checkbox();		
		// ������ ������ �����:
		Cb=new Checkbox(" ����� ",true);
		Cb.setBounds(5,260,getWidth()-10,30); // ������
		add(Cb);
		
		// ������ ������ ("���������"):
		B1=new Button("���������");
		// ������ ������ ("�������"):
		B2=new Button("�������");
		// ������� � ��������� ������ ������:
		B1.setBounds(5,getHeight()-75,getWidth()-10,30);
		// ������ � ��������� ������ ������:
		B2.setBounds(5,getHeight()-35,getWidth()-10,30);
		add(B1);      // ���������� ������ ������ �� ������
		add(B2);      // ���������� ������ ������ �� ������
	}

}
		
	// ����� ������ ��� ����������� �������:
	class PPanel extends Panel{
		// ������ �� ������ ���������� ������� �������:
		public Plotter G;		
		
		// ���������� ����� ��� ���������� ������� �������:
		class Plotter{
			// ������� ��������� ��������� ���������:
			private double Xmin, Xmax, Ymin, Ymax;
			// ��������� ������ ������ �����:
			private boolean status;
			// ���� ��� ����� �������:
			private Color clr;
			// ���� ��� ����������� ����� �����:
			private Color gclr;
			// ����������� ������
			// (��������� - ������ � �������� � ������ ��� ����������� �������):			
			
			int size;
			Double masx[];
			Double masy[];			
			
			Plotter(BPanel P, ArrayList<Double> xd, ArrayList<Double> yd){			
				
				setDataGraph(xd, yd);				
				
				status=P.Cb.getState();
				    	
				//����� ������ ����� 	
				gclr=Color.GRAY;		    		
				
				clr=Color.BLUE;
			}		
		
			
		public void setDataGraph(ArrayList<Double> xd, ArrayList<Double> yd){
			size = xd.size();
				masx = xd.toArray( new Double[xd.size()] );				
				masy = yd.toArray( new Double[yd.size()] );				
				
				Xmin = masx[0];
				Xmax = masx[0];
				Ymin = masy[0];
				Ymax = masy[0];
				
				for(int i = 1; i<size; i++){
					if (Xmin>masx[i]) Xmin = masx[i];
					if (Xmax<masx[i]) Xmax = masx[i];
					if (Ymin>masy[i]) Ymin = masy[i];
					if (Ymax<masy[i]) Ymax = masy[i];
				}
		}	

		// ����� ��� ���������� � ����������� ��������:
		public Plotter remember(BPanel P, ArrayList<Double> xd, ArrayList<Double> yd){
			return new Plotter(P, xd, yd);	
	}
	
	// ����� ��� ����������� ������� � �����
	// (Fig - ������ ������������ ���������):
	public void plot(Graphics Fig){		
				
		// ��������� ������� ����������� �������:
		int H,W,h,w,s=20;//s ������� ����������� 100 - small, 20 - stand.
		H=getHeight();
		W=getWidth();
		h=H-2*s;
		w=W-2*s;
		
		// ������� ������� �������:
		Fig.clearRect(0,0,W,H);
		
		// ��������� ���������� � ���������� ����� �����:
		int k,nums=10;
		
		// ���� ������������ ���� - ������:
		Fig.setColor(Color.BLACK);
		
		// ����������� ������������ ����:
		Fig.drawLine(s,s,  s,  h+s);
		Fig.drawLine(s,s+h,s+w,s+h);
		
		double Xsdvig = 0; //��� ����������� ������������� �������� �� �������������
		double Ysdvig = 0;
				
		//�������� �� ������ �������������� x
		if (Xmin<0) Xsdvig = Math.abs(Xmin);		
		//�� ����� ������ ������� �� ������ ���������
		if (Xmin>0) Xsdvig = -Xmin;
		
		//�������� �� ������ �������������� y
		if (Ymin<0) Ysdvig = Math.abs(Ymin);
		//����� �� ������� ����� ������ 0 � ����������� ������.1
		if (Ymin>0) Ysdvig = -Ymin;		
		
		// ����������� ������� � �������� �������� �� ������������ ����:
		for(k=0;k<=nums;k++){
			Fig.drawLine(s+k*w/nums,s+h,s+k*w/nums,s+h+5);
			Fig.drawLine(s-5,s+k*h/nums,s,s+k*h/nums);
			
			Double Xviz = new BigDecimal(Xmin+k*(Xmax-Xmin)/nums).setScale(1, RoundingMode.HALF_UP).doubleValue();
			Fig.drawString(Double.toString(Xviz),s+k*w/nums-5,s+h+15);			
			
			Double Yviz = new BigDecimal(Ymin+k*(Ymax-Ymin)/nums).setScale(1, RoundingMode.HALF_UP).doubleValue();
			//y
			Fig.drawString(Double.toString(Yviz),s-17,s+h-1-k*h/nums);		
		}	
		
		
		
		// ����������� ����� (���� ���������� ������):
		if(status){
			Fig.setColor(gclr);
			// ����������� ����� �����:
			for(k=1;k<=nums;k++){
				Fig.drawLine(s+k*w/nums,s,s+k*w/nums,h+s);
				Fig.drawLine(s,s+(k-1)*h/nums,s+w,s+(k-1)*h/nums);
			}
		}
		
		
		// ����������� �������:
		Fig.setColor(clr);     // ��������� ����� �����
		// ������� �� ���� ������� �� ������ �� ���������:
		double dx=(Xmax-Xmin)/w,
				dy=(Ymax-Ymin)/h;
		
		// ���������� ��� ������ ���������� ���������:
		double x2,y2;
		
		// ���������� ��� ������ ��������� � ���� ����������� �������:
		int h1,h2,w1,w2;
		
		// ��������� ��������:
	
		w1=s+(int)Math.round((masx[0]+Xsdvig)/dx);
				
		h1=h+s-(int)Math.round((masy[0]+Ysdvig)/dy);
		
		// ��� � �������� ��� ������� �����:
		double step = masx[1] - masx[0];//5;
		
		int j = 1;

		//debug code
		//System.out.println("deboug H,W,h,w, " + H +" "+ W +" "+ h +" "+ w + " Ysdvig"+Ysdvig+ " dy"+dy+" s"+s);
		
		// ����������� ������� ����� � ���������� �� �������:
		for(double i=masx[1]; i<=Xmax; i+=step){
						
			//x
			x2=i;//i*dx;
			w2=s+(int)Math.round((x2+Xsdvig)/dx);			
			
			//y
			y2=masy[j];//f(x2);
			h2=h+s-(int)Math.round((y2+Ysdvig)/dy);		
			
			// �����:
			Fig.drawLine(w1,h1,w2,h2);
			
			// ������� ����� (�������):
			Fig.drawRect(w1-2,h1-2,4,4);

			//debug code
				//Double Ddeb = new BigDecimal(masx[j-1]).setScale(1, RoundingMode.HALF_UP).doubleValue();
				//String deb1 = "x"+Ddeb;
				//Ddeb = new BigDecimal(masy[j-1]).setScale(1, RoundingMode.HALF_UP).doubleValue();
				//String deb2 = "Y"+Ddeb;
				//Fig.drawString(deb1, w1, h1+15);
				//Fig.drawString(deb2, w1, h1+30);
			
			// ����� �������� ��� ���������:
			w1=w2;
			h1=h2;
			j++;
		}
		
		Fig.drawRect(w1-2,h1-2,4,4);
		
	}
}
							
		
		ArrayList<Double> xd;
		ArrayList<Double> yd;
		
		// ����������� ������
		// (��������� - ���������� � ������� ������,
		// � ����� ������ �� ������ � ��������):
		PPanel(int x,int y,int W,int H,BPanel P, ArrayList<Double> xd, ArrayList<Double> yd){
			
			this.xd = xd;
			this.yd = yd;					
			// �������� ������� ���������� ������� �������:
			G=new Plotter(P, xd, yd);
			// ���� ���� ������:
			setBackground(Color.WHITE);
			// ������ � ��������� ������:
			setBounds(x,y,W,H);
		}
		
		// ��������������� ������ ����������� ������:
		public void paint(Graphics g){
			// ��� ����������� ������ ���������� �����
			// ��� ����������� �������:
			G.plot(g);
		}
}
		
	// ����� ��� ������ �������:(�� ���������)
	// ����� ��� ������ �������:
	class HPanel extends Panel{
		// �����:
		public Label L;
		// ��������� �������:
		public TextArea TA;
		// ����������� �������� ������
		// (��������� - ���������� � ������� ������):
		HPanel(int x,int y,int W,int H){
			// ���� ���� ������:
			setBackground(Color.LIGHT_GRAY);
			// ������ � ��������� ������:
			setBounds(x,y,W,H);
			// ���������� ��������� ���������� ����������� ������:
			setLayout(null);
			// ����� ��� ������ �������:
			L=new Label("�������",Label.CENTER);
			// ������ � ��������� �����:
			L.setBounds(0,0,W,20);
			// ���������� ����� �� ������:
			add(L);
			// ��������� ������� ��� ������ �������:
			TA=new TextArea("�������� ������� ������� ����� ������� �����������, "
					     +"\n��� ��� ������� ���������� ��������� ������� "
					     + "\n������������ ������ ������.");
			// ����� ��� ��������� �������:
			TA.setFont(new Font("Serif",Font.PLAIN,14));
			// ������ � ��������� ��������� �������:
			
			TA.setBounds(5,20,W-10,60+20);
			// ������� ���������� ��� ��������������:
			TA.setEditable(false);
			// ���������� ��������� ������� �� ������ �������:
			add(TA);
		}
	}
	
	// ����� ����������� ��� ������ ������:
	class Button1Pressed implements ActionListener{
		
		// ������ � ��������:    
		private BPanel P1;
		
		// ������ ��� ����������� �������:
		private PPanel P2;
		
		ArrayList<Double> xd;
		ArrayList<Double> yd;
		
		PlotData PD;
		boolean InOrOut;
		
		// ����������� ������ (��������� - ������):
		Button1Pressed(BPanel  P1,PPanel P2, PlotData PD, boolean InOrOut){			
			
			this.InOrOut = InOrOut;
			this.PD = PD;
			this.P1=P1;
			this.P2=P2;
			
		}
		
		// ����� ��� ��������� ������ �� ������:
		public void actionPerformed(ActionEvent ae){
			PD.setDataForGrapf(P1.Ch[0].getSelectedItem()+"\\outlagrang.xls");			
			
			if (InOrOut){
				this.xd = PD.x;
				this.yd = PD.y;
			}else{
				this.xd = PD.xpoly;
				this.yd = PD.ypoly;
			}			
	
			// ���������� ���������� (��������) ��� ����������� �������:
			P2.G=P2.G.remember(P1, xd, yd);
			
			// ������� �� ������ (���������� �������):
			P2.G.plot(P2.getGraphics());			
			
		}
		
	}
		
	// ����� ����������� ��� ������ ������:
	class Button2Pressed implements ActionListener{
		
		// ����� ��� ��������� ������ �� ������:
		public void actionPerformed(ActionEvent ae){
			// ������� �� ������:P
			System.exit(0);
		}
		
	}
		
		// ����� ����������� ��� ������ ������ �����:
	class cbChanged implements ItemListener{
		
			
		// ����������� ������ (�������� - ������ � ��������):
		cbChanged(BPanel P){}
	
		// ����� ��� ��������� ��������� ��������� ������:
		public void itemStateChanged(ItemEvent ie){
			// ������� �� ��������� ��������� ������:
			System.out.println("���. ������ �����");			
		}	
	}			
		
	// Label Panel - ����� ��� ������ ��������� �����
	class LPanel extends Panel{
			
		// �����:
		public Label L;
			
		//����������� �������� ������
		// (��������� - ���������� � ������� ������):
		LPanel(int x,int y,int W,int H, String s){				 
			// ������ � ��������� ������:
			setBounds(x,y,W,H);
			// ���������� ��������� ���������� ����������� ������:
			setLayout(null);
			// ����� ��� ������ �������: 
			L=new Label(s,Label.CENTER);
			// ������ � ��������� �����:
			L.setBounds(0,0,W,20);
			// ���������� ����� �� ������:
			add(L);							
		}		
	}