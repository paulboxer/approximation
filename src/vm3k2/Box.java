package vm3k2;

import java.util.ArrayList;
import Jama.Matrix;

//����� ���������� ��������� 
public class Box extends Kernel{
		
	//����������� ����������� ������ ������������
	//3 - ��������
	//4 - ������������ (����������)
	//5 - ����������
	int size;
	int staticsize;
	int type;
	double step;
	
	private double[][] x; //x ������� 3.1 
	private double[][] y;//y ������� 3.1
	
	//double, � �� Double ��� �������� ������ � ����� ������� �������
	private double[] sx;//����� �������� x[i] � ������� 3.1 
	private double[] sy;//����� �������� y[i] � ������� 3.1
	private double[][] pnp; //�������� �������� � ������� ������ (poli nodal point)
	private double spnp;
	
	private Matrix G[];//�-�� �����
	private double[] D;//������ ����������� ������������ (�������������) 
	private double[] B; //������� ��������� ������
	private double[] C; //������������
	private double S;//�����������
	
	//������ ������� �� �������������� ����. 
	ArrayList<Double>  allpolyintervalx = new ArrayList<Double>(); //x � �������� �����
	ArrayList<Double>  allpolyintervaly = new ArrayList<Double>(); //��������� ���������� �������� (��������, ������������, ����������)
	
	double startx;
	double finishx;
	
	public Box(Kernel k){
		outbox = k.outbox;
		debuglog = k.debuglog;
		
		n = k.getx().length;	
		size = 5;//� ��������� ������ ������ ���� ������ 5
		staticsize = size;
		
		x = new double[n][size+1];
		y = new double[n][size-1];
		sx = new double[size+1];
		sy	= new double[size-1];		
		pnp = new double[n][2];//new double[n][size-1];
		
		setxymaszeroaddr( getdoublearr(k.getx()), getdoublearr(k.gety()), n );//��������� ������� ������ �������� ��������� �������	
		finishx = x[n-1][0];
		calctablex();
		calctabley();		
		calcsx();
		calcsy();		
		print("");
	}	
	
	public void GeneralProcessing(String type){
		
		if (type == "��������") 	 this.type = 3;
		if (type == "������������") this.type = 4; 
		if (type == "����������") 	 this.type = 5;
				
		size = this.type;		
		init(size);//���������� ������ ������������ ��������
		calcfirstG(size);//������������ ������ G		
		calcB(size);
		calcG(size);//��������
		calcD(size);//���������� ������������
		calcC(size);//���������� ������������		
		calcallpnp();
		calcsallpnp();
		calcS();
		calcofallpolyinterval(0.1d, size);		
	}
	
	public void init(int type){
		G = new Matrix[type];
		D = new double[type];
		B = new double[type-1];
		C = new double[type-1];	
		allpolyintervalx.clear();
		allpolyintervaly.clear();
	}	
	
	//��������������� Double[] � double[]
	private double[] getdoublearr(Double[] Doublearr){
		int n = Doublearr.length;
		double[] doublearr = new double[n];
		int i = 0;
		while (i<n){
			doublearr[i] = Doublearr[i];
			i++;
		}
	return doublearr;
	}
	
	//���������� ������� �������� �������� x � y
	private void setxymaszeroaddr(double[] x, double[] y, int n){
		int i=0;
		while(i<n){
			this.x[i][0] = x[i];
			this.y[i][0] = y[i];
			i++;
		}
	}

	//���������� ������� x[]
	private void calctablex(){
		int i=1;
		int j=0;
		while(i<size+1){//������� �������
			while(j<n){//������� ������
				x[j][i] = x[j][0] * x[j][i-1];
				j++;				
			}
			j=0;
			i++;
		}		
	}
	
	//���������� ������� y[]
	private void calctabley(){
		int i=1;
		int j=0;
		//double[] buff = new double[n];
		while(i<size-1){
			while(j<n){
				y[j][i] = y[j][0] * x[j][i-1];
				j++;
			}
			j=0;
			i++;
		}
	}	
	
	//����� ��������� ������� x[]
	private void calcsx(){
		int i = 0;
		int j = 0;
		double summ = 0;
		while(i<size+1){
			while(j<n){
				summ += x[j][i];
				j++;
			}
			sx[i] = summ;
			summ = 0;
			j=0;
			i++;
		}
	}	
	
	//calcsy();//����� ��������� ������� y[]
	private void calcsy(){
		int i = 0;
		int j = 0;
		double summ = 0;
		while(i<size-1){
			while(j<n){
				summ += y[j][i];
				j++;
			}
			sy[i] = summ;
			summ = 0;
			j=0;
			i++;
		}
	}
	
	//���������� ������� G[]
	private void calcfirstG(int type){		
		double[][] G = new double[size-1][size-1];  
		G[0][0] = n;
		int i = 0;
		int j = 1;
		int k = -1; //�����������
		while(i<type-1){//stolbec
			while(j<type-1){//stroka
				G[j][i] 
						=	
						sx[j+k];				
				j++;				
			}
			k++;
			j=0;
			i++;
		}
		this.G[0] = new Matrix(G);
	}	
	
	public void calcB(int type){
		int i = 0;
		while(i<type-1){
			B[i] = sy[i];
			i++;
		}		
	}	
	
	//������ �������
	public void calcG(int type){		
		int m = 1;
		int i = 0;
		int j = 0;
		while(m<type){//����� �������			
			G[m] = G[0].copy();
			while(j<type-1){//����� ������					
					G[m].getArray()[j][i] = B[j];//i ����� �������
					j++;
				}				
				j=0;
				m++;
				i++;
		}
	}
	
	//���������� ������������
	public void calcD(int type){
		int i = 0;
		while(i<type){
			D[i] = G[i].det();
			i++;
		}		
	}	
		
	//calcC();//���������� ������������� C[]
	private void calcC(int type){
		int i = 0;//debug only 3 value 0 1 3
		while(i<type-1){
			C[i] = D[i+1]/D[0];
			i++;
		}		
	}
	
	//���������� �������� (type = size)
	private double calcPoli(int type, double xvalue){
		//debag step summ 0 1 2 size-1
		double summ = 0;
		int i=0;
		int degree = 0; //�������
		while(i<size-1){
			summ += C[i]*Math.pow(xvalue, degree);
			i++; degree++;
		}
		return summ;
	}
	
	//���������� ������� �������� � ������� ������ (pnp[n][size-1])
	private void calcallpnp(){
		int i = 0;
		int j = 1;
		int degree = 2;
		
		//���������� ������� ������� pnp
		while(i<n){
			pnp[i][0] = calcPoli(type, x[i][0]);
			i++;
		}
		
		//���������� ���������� ������� pnp
		i=0;		
			while(i<n){
				pnp[i][j] = Math.pow(   (y[i][0] - pnp[i][0]),   degree   );
				i++;
			}			
			i=0;		
	}
	
	//����� ������� ��������� � ������� ������.
	public void calcsallpnp(){
		int j=1;
		int i=0;
		spnp = 0;
			while(i<n){//������
				spnp+=pnp[i][j];
				i++;
			}
			i=0;
	}
	
	//������ ����������� S
	private void calcS(){
		S = Math.sqrt((spnp/n)); //�� n+1, ��� ��� n ��� � ���� n+1 � ������� ������� �� ���� ��� � ���.
		print("S="+S);
	}	
	
	//���������� ���� ��������� �� ���������
	public void calcofallpolyinterval(double step, int type){
		this.step = step;
		int i=0;//������
		startx = x[0][0];			
		while(startx <= finishx){					
			allpolyintervalx.add(startx);				
			allpolyintervaly.add ( calcPoli(type, allpolyintervalx.get(i)  ) );
			startx+=step;
			i++;				
		}
		i=0;						
	}	
	
	private void outln(Object object){
		outbox.println(object);//outbox ����������� �� textfile
		print(object);//����� print ����������� �� kernel
	}
	
	private void out(Object object){
		outbox.print(object + "	");
		print(object);
	}
	
	//������������� ��������� � ����
	public void WriteSubtotalResultFile(){		
		outln("\n������� 3.1.1 - ��������������� ������� x");//��������� �����		
		outln("x	x^2	x^3	x^4	x^5	x^6");
			int i = 0;
			int j = 0;	
			int outsumm = 1;
			while(i<n+outsumm){//string
				while(j<size+1){//column				
					if (i<n) out(dp(x[i][j]));//����� dp ����������� �� delpoint
					else{
						if (j==0) outln("����� ��������");
						out(dp(sx[j]));
					}
					j++;
				}
				outln("");
				j=0;
				i++;
			}			
		
		outln("\n������� 3.1.2 - ��������������� ������� y");//��������� �����			
		outln("y	xy	x^2*y	x^3*y");
			i = 0;
			j = 0;			
			while(i<n+outsumm){//string
				while(j<size-1){//column
					if (i<n) out(dp(y[i][j]));
					else{
						if (j==0) outln("����� ��������");
						out(dp(sy[j]));
					}
				j++;								
				}
			outln("");
			j=0;
			i++;
			}		
		
		out("\n�������� ��������, ������������ � ���������� ������������ � ������ �����������");			
		}	
		
		public void WriteResultFile(){		
		outln("\n");		
		String s = "******************************************************************************";	
		switch (type){		
		case 3: outln("������ �������� ������������     "+s); break;
		case 4: outln("������ ������������ ������������ "+s); break;
		case 5: outln("������ ���������� ������������   "+s); break;
		}	
	
		outln("\n������������ ������");
		Integer i = 0;
		s = "";
		while (i<D.length){
			outln("D"+s+" =	"+dp(D[i]));
			s = i.toString();
			i++;
		}
		
		outln("\n������������");
		i=0;
		while (i<C.length){
			outln("C"+i+" =	"+dp(C[i]));			
			i++;
		}
						
		outln("\n�����������");
		outln("S =	"+dp(S));
		
		int k = type - 3;
		outln("\n������� 3.2."+(2+k)+" ��������� � ������� ������");
		outln("p(x)	(y-p(x))^2");
		int j=0;
		i = 0;
		while(i<pnp.length){//string
			while(j<pnp[0].length){//column
				out(dp(pnp[i][j]));
				j++;
			}
		outln("");
		j=0;
		i++;
		}
			
		outln("\n������� 3.3."+(1+k)+" �������� � ����� "+step);
		out("x");
		for (Double item : allpolyintervalx){
			out(dp(item));		
		}
		outln("");
		out("y");
		for (Double item : allpolyintervaly){
			out(dp(item));		
		}		
	}
}