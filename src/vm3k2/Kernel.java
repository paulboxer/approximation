package vm3k2;

import java.io.*;//��� PrintWriter

//����, ���������� ������� �������, � main() ��������� ���������� ���� ������� 
public class Kernel extends TextFile{
	public Double[] x;
	Double[] y;
	Double x0,h,til,tinup,tindown;//til,tin,tib - ����� ������������ -�������, ������ , �������
	Integer n;//n - ���������� �������� �����
	private TextFile tf; 
		
	public //���������, ����� ����� ��������� � �������� ������ ���� ������ � ������������ ����������� ������!
		Kernel(){}//����������� ������ ����, ��� ������������ - lagrang(kernel k) ������!
	
	public Kernel(String s){}
	
	Kernel(String OutFolderName, String InFileName){	
		tf = new TextFile(createFolder(OutFolderName));//������� �������� ������� � �������� �����;
		SetPointersTextfile();			
		int i = 0;
		n = 10;
		x = new Double[n];
		y = new Double[n];		
		//����� ��������
		String s = readonesymbol(InFileName);//readonesymbol �������������� ����� textfile
		print("����������� ������� " + s);					
		readonesymbol(InFileName);//x		
		while(i<n){//x[]
			s = readonesymbol(InFileName);
			x[i]=Double.parseDouble(s);
			i++;
		}			
		readonesymbol(InFileName);//y			
		i=0;
		while(i<n){//y[]
			s = readonesymbol(InFileName);
			y[i]=Double.parseDouble(s);
			i++;
		}		
		readonesymbol(InFileName);//til
		s = readonesymbol(InFileName);
		til = Double.parseDouble(s);			
		readonesymbol(InFileName);//tinup
		s = readonesymbol(InFileName);
		tinup = Double.parseDouble(s);//String in Double			
		readonesymbol(InFileName);//tindown
		s = readonesymbol(InFileName);
		tindown = Double.parseDouble(s);		
		readonesymbol(InFileName);//close infile			
	}
	
	Double[] getx() {return x; }
	Double[] gety() {return y; }
	Double   gettil(){return til;}
	Double   gettinup(){return tinup;}
	Double   gettindown(){return tindown;}
	
	public void writebegindatafile(PrintWriter file){
		file.println("������� 0.1. - ��������� ������� index, x, y");//��������� �����
		writefile(file, 0, n);//������ - ������� ��������
		writefile(file, x, 0, n);//������ - �������
		writefile(file, y, 0, n);
		file.println("");
	}	
		
	public void print(Object s){
		System.out.println(s);
		debuglog.println(s);
	}

	//����������� ����� � �������
	public static void con(Object s){
		System.out.println(s);	
	}
	
	private void SetPointersTextfile(){
		outlagrang = tf.outlagrang;
		outnewton = tf.outnewton;
		outbox = tf.outbox;
		debuglog = tf.debuglog;
	}
	
			
	public void GeneralProcessing(){
		print("����� ���������");	
				
		writebegindatafile(outlagrang);//��������� ������� ������� � �������� �����
		writebegindatafile(outnewton);
		writebegindatafile(outbox);
		writebegindatafile(debuglog);
				
		//�������
			//��������� ������� � ����c �������
			//��������� �������� ������ ��������, � ������� ����� ��� ������ �����������
			//����� � ������� ���������� ����������  laplpolipogr()
			//���������� �����������
			//��������� ����������� �������
			//�������� ��� ������� ����������� �������
			Lagrang l = new Lagrang(this).polipogr().controlpolipogr().pogr().optimals().controlfinals();
			//������ ���������� ���������� ������������ � ����
			l.writeresultfile();	
			l.finalpoli(0.25).writeresultfile_step_0_25();//������ ��������� � ����� 0.25
			l.finalpoli(0.1).writeresultfile_step_0_1();//������ ��������� � ����� 0.1
	
		//������		
/**/		Newton ntn = new Newton(this);		
			//forward (������)		
/**/			ntn.dy_2_forward();//���������� ����� �������		
/**/			ntn.indexfortin(tinup, "������");//���. ����. ����� � ����� ������������
/**/			ntn.calcq(tinup, x);//���������� q
/**/			ntn.calcLfp(ntn.getdy_2(), ntn.getLfp());//������ ��������� � ����� ����� ��� ������ �����������
/**/			ntn.calcallR(ntn.getdy_2(), ntn.getR());//������ ������������
			//ago (�����)			
/**/			ntn.indexfortin(tindown, "�����");//���. ����. ����� � ����� ������������
/**/			ntn.calcq(tindown, ntn.getx());//���������� q
/**/			ntn.calcLfp(ntn.getdy_2(), ntn.getLfp_b());// ����� ������ ��������� ��� ������ ������������ 
/**/			ntn.calcallR(ntn.getdy_2(), ntn.getR_b());// ����� ������ ������������
/**/			print("����������� ���������\n");
/**/			ntn.setypogr(ntn.getR());//���������� ������� ������������ ��� ��������� �������������� �������
/**/			ntn.optimals();//����� ������������ ������� ��� ������
/**/			ntn.calcLsteptrend(0.25, "������");//���������������� ���� ������� � ��������� ����� ������
/**/			ntn.setypogr(ntn.getR());//����� ������������ ������� ��� �����
/**/			ntn.optimals();//������������� �������������� �������� ������ �������
/**/			ntn.calcLsteptrend(0.25, "�����");//���������������� ���� ������� � ��������� ����� �����			
			//����� ������� � ������������
/**/			ntn.writetofile("������", outnewton);
/**/			ntn.writetofile("�����", outnewton);			
			//����� ��������� �� ��������
/**/			ntn.writetofile(outnewton, "������");
/**/			ntn.writetofile(outnewton, "�����");			
			
		//��������
		print("������������ ������� ���������� ���������");
		Box b = new Box(this);
		b.WriteSubtotalResultFile();		
		b.GeneralProcessing("��������");
		b.WriteResultFile();		
		b.GeneralProcessing("������������");
		b.WriteResultFile();		
		b.GeneralProcessing("����������");
		b.WriteResultFile();			
		
		outlagrang.println("\n lagrang end out");
		outnewton.println("\n\n newton end out");
		outbox.println("\n box end out");
		
		close();//��������� �������� �����
	}		
}