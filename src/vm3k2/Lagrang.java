package vm3k2;

import java.math.BigDecimal;
import java.math.RoundingMode;


//������� ��������
public class Lagrang extends Kernel{ 
	private Double ypoli[];
	Double ypogr[];//������ ���������, ������ ������������ (Double ����� �������� ypogr[i] = null)
	private Double  fpx[], fpy[];//finalpolinom ������ ��������� ����������� �� ��� ���������
	int opts;//����������� �������
	int a;//����� ���������� ��������
	int dopk = 0; //�������������� ����������� ����������� ��� ����������� ���������� b, ��� ���������� �������� �� ���������.
	
	Lagrang(){}
	
	Lagrang(Kernel k){
		//���������� �������� �������������� ����������
		outlagrang = k.outlagrang;
		debuglog = k.debuglog;
		
		x =  k.getx();
		y =  k.gety();
		n =  x.length;
		
		til = k.gettil();
		
		ypoli = new Double[n];
		ypogr = new Double[n];		
		a = n;
	}
	
	public Lagrang polipogr(){
		print("���������� �������� � ����� �����, � ������ ��������");
		for(int i=1; i<n; i++){//i 1..9
			ypoli[i] = poli(i);
		}
		return this;
	}	
	
	//������� ���������� �������� ��������
	public double poli(int s){		
		//������������ ���������� ti x[] y[] a
		//s - ������� ��������
		int b = a - s - 1 + dopk; //����������� ������� ��� �������� ������ (����� �������� ������ ������)		
		double pr = 1; //������������
		double summ = 0; //�����
		if (s==0) s=1;
		print("\n������� �������� "+s+"----------------------------------");
		for(int i = 0; i <= s; i ++){
			for(int j = 0; j <= s; j++){
				if (i != j){
					pr = pr*((til-x[j+b])/(x[i+b]-x[j+b]));
					print("i="+i+" j="+j+" �����="+((til-x[j+b])/(x[i+b]-x[j+b])));
				}				
			}
			print("��� ����� "+pr);
			pr = pr*y[i+b];
			print("��� �����*y (y="+y[i+b]+")  ="+pr+ "   b="+b);
			summ += pr;
			print("summ "+summ+"\n");
			pr = 1;
		}
		print("��������� �������� ������� "+s+" y="+summ+"\n");
		return summ;
	}
	
	public Lagrang controlpolipogr(){
		print("�������� ���������� �������� � ����� �����, � ������ ��������");
		for(int i=1; i<n; i++){
			System.out.print("������� = "+i);
			print(" y="+ypoli[i]);			
		}
		return this;
	}
	
	//���������� ������������
	public Lagrang pogr(){
		int ev = n;//endvalue
		int sv = 0;//startvalue		
		//��������� �������
		int i=0;
		while(i<ev){
			ypogr[i] = null;
			i++;
		}
		//���������� �����������
		sv=2;
		while(sv<ev){
			ypogr[sv] = Math.abs(ypoli[sv-1]-ypoli[sv]);
			sv++;
		}
		return this;
	}
	
	//����������� ������������ �������
	public Lagrang optimals(){
		//����� ������������
		int i = 0;
		int ad = 0; //�����
		Double buffer = null;
		while(i<n){
			if (ypogr[i]!=null){
				if (buffer==null) {buffer=ypogr[i]; ad = i;}
				else if (buffer>ypogr[i]) {buffer = ypogr[i]; ad = i;}
			}
			i++;
		}
		opts = ad;
		print("����������� ������� opts="+opts);
		return this;
	}
	
	//������� ����������� ���������� ������� ��� ������ �����
	public Integer finals(Double point, Double x[]){
		Integer mi = 0;//maximalindex ���������� ����� ���������
		int i=1;
		while(i<n){
			if (x[i]!=null){
				if (point <= x[0]) {
					print("finals() �� ���������� ������������� point="+point+" i="+i);
					return null;
				}
				else 
				if ( x[(i-1)] < point&&point <= x[i] ) mi = i;
			}							
			i++;
		}
		a = mi;//����������� ����� ���������� �������� ��� ������ �����
		
		//�������� ����������� � ��������� (�������)
		Integer s = null;		
		if (mi>=opts) s=opts; //���� ����� ������������������ ���������, �� ������� ����� ������������
		if (mi<opts) s=mi;//���� ����� ������ ������ ����� �������, ����� ������� ����� ����� ������
		//���������� �������� ������� s<a, ����� �������� ������ �� ����� ������� �������
		boolean en = true;
		while(en){
			if (s>=a) s-=1; else en = false;
		}		
		return s;
	}
	
	//test for finals()
	public Lagrang controlfinals(){
		Double p = x[0];
		print("test finals()");
		while(p<=x[n-1]){			
			print("point="+p+" neobhodimporadok="+finals(p, x));
			p+=0.25;
		}
		return this;
	}
	
	//���������� ������ ����� �� ���� ��������� � �����
	public Lagrang finalpoli(Double step){
		dopk = 1;
		print("\n\nfinalpoli(step) step="+step);
		
		//���������� ������� ���������
		Double xstart=x[0];
		Double xend=x[n-1];
			
		//���������� �������� ������
		int size = (int) ((xend - xstart)/step)+1;
		fpx = new Double[size];
		fpy = new Double[size];
		int i=0;//1;
		int s=0;//���������� �������
		fpx[0]=x[0];
		Integer index;
		while(i<size){
			if (i==1){
				print("");
			}
			
			//���������� ������� 
			fpx[i] = xstart;
			xstart = xstart + step;
		
			index = findelement(fpx[i], x);//���� ���� �� ����������
			if (index != null) fpy[i]=y[index];
			else{//���������� �� ����� ����� ����������� �������
				s = finals(fpx[i], x);//i = 1 s = 1 a = 1//
				til=fpx[i];				
				fpy[i]=poli(s);
			}
			i++;
		}
		dopk = 0;
		return this;
	}	
	
	Integer findelement(Double element, Double x[]){
		Integer index = null;
		int i=0;
		while(i<x.length){
			element = new BigDecimal(element).setScale(3, RoundingMode.HALF_DOWN).doubleValue();
			if (element.doubleValue() == x[i].doubleValue()) index = i;
			i++;
		}		
		return index;
	}	
	
	public void writeresultfile(){				
		outlagrang.println("\n������� 1.1. - ������� ��������, �������, �����������");//��������� �����
		//writefile() - ������������ �� kernel, a kernel ����������� �� �� textfile
		writefile(outlagrang, 1, n);//������ - ������� ��������
		writefile(outlagrang, ypoli, 1, n);//������ - �������
		writefile(outlagrang, ypogr, 2, n, 1);//������ - ����������� (����� �� ���������)
		//����� ������������ �������
		outlagrang.println("\n��������������� ����������� ����� ����������� ������� ������������ = "+opts);
	}
	
	public void writeresultfile_step_0_25(){
		//����� ��������� ������� � ����� 0,25
		outlagrang.println("\n������� 1.2. - ����������������� ������� � ����� 0,25 index, x, y");
		writefile(outlagrang, 0, fpx.length);//��������� ��������
		writefile(outlagrang, fpx, 0, fpx.length);
															//if(fpy[0]==null) fpy[0]=0d;
															//fpy[1] = (y[0]+y[1])/2;
		writefile(outlagrang, fpy, 0, fpy.length);			
	}
	
	public void writeresultfile_step_0_1(){
		//����� ��������� ������� � ����� 0,1
		outlagrang.println("\n������� 1.3. - ����������������� ������� � ����� 0,1 index, x, y");
		writefile(outlagrang, 0, fpx.length);//��������� ��������
		writefile(outlagrang, fpx, 0, fpx.length);
															//if(fpy[0]==null) fpy[0]=0d;															
															//fpy[1] = ((y[0]+y[1])/3);
															//double k = 0;
															//if (fpy[0]>0) k = fpy[0];
															//fpy[1] = ((fpy[0]+fpy[4])/3)+ k/2;
															//fpy[2] = (fpy[1]+fpy[3])/2;
															//fpy[3] = (fpy[2]+fpy[4])/2;
															
		writefile(outlagrang, fpy, 0, fpy.length);
	}
	
}
