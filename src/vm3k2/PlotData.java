package vm3k2;

import java.util.ArrayList;

//�������� �������� ������	
public class PlotData extends TextFile {
	
	private ArrayList<String>ALS;//���� ����
	ArrayList<Double> x;
	ArrayList<Double> y;
	ArrayList<Double> xpoly;
	ArrayList<Double> ypoly;
	String lFold[]; //������ ��������� � �������� ������� ������
	
	private String path;
	private String outFoldName;
	private String outFileName;
	
	public PlotData(){		
		lFold = GetListFoldInOutputFolder();		
		path = System.getProperty("user.dir");
		path += "\\OutFolder\\";
		outFoldName = lFold[0]+"\\";
		outFileName = "outlagrang.xls";
		
			ALS = new ArrayList<String>();//���� ����
			x = new ArrayList<Double>();
			y = new ArrayList<Double>();
			xpoly = new ArrayList<Double>();
			ypoly = new ArrayList<Double>();			
			
	}
	
	static void print(Object object){
		System.out.println(object);
	}	
	
	
	private ArrayList<Double> getNextSimbol(String lineNumbers){
		ArrayList<Double> arrld = new ArrayList<Double>();
		char arrch[];
		arrch = lineNumbers.toCharArray();
		int i = 0;
		String str = "";
		int length = arrch.length;
		while(arrch[i]!='\n'){			
			
			while(arrch[i]!='\t' && arrch[i]!='\n'){
				str+=arrch[i];
				i++;
			}
			i++;
			if (length<=i) break;
			arrld.add(Double.valueOf(str.replaceAll(",", ".")));
			str = "";
		}
		return arrld;
	}	
	

	//������� �������� ������ ��� ������� (��������� ��� �����)
		public void setDataForGrapf(String FoddAndFileName){
			
			ALS = read(path + FoddAndFileName);
			//C:\YandexDisk\java\workspace\vm3k2\OutFolder\�������00\outlagrang.xls
			x = getNextSimbol(ALS.get(2));//x[]
			y = getNextSimbol(ALS.get(3));//y[]
			
			xpoly = getNextSimbol(ALS.get(20));//xpoly[]
			ypoly = getNextSimbol(ALS.get(21));//ypoly[]
			
		}		
	
	 void GeneralProcessing(){		 
		setDataForGrapf( outFoldName + outFileName );		
		new PlotFrame(700,550, this);			
	}	
	
	public static void main(String[] args) {		
		new PlotData().GeneralProcessing();		
	}

}


	

