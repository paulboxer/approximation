package vm3k2;

import java.util.*;//for ArrayList
import java.io.*;//��� PrintWriter

public class Newton extends Lagrang{
	//������
	private Double dy_2[][];//dy ��������� ������ ������
	private Double Lfp[];//L for pogr ������ ��������� ��� ������ �����������
	private Double R[];//������ ������������
	private Double X_forward[];
	private Double L_forward[];//��������� ������������ � ������������ �����	
	//�����
	private Double Lfp_b[];//L for pogr ������ ��������� ��� ������ ����������� back
	private Double R_b[];//������ ������������ back
	private Double X_ago[];
	private Double L_ago[];//��������� ������������ � ������������ �����	
	//�����
	private Double q;
	private Integer index;
	private Boolean valve;//1 - ������, 0 - ����� 
	private int possibles;//����������� � ���������� ������� ������������	
	
	Newton(Kernel k){
		super();//����������� ������������� ������
		//���������� �������� �������������� ����������
		debuglog = k.debuglog;
		x =  k.getx();
		y =  k.gety();
		n =  x.length;
		tinup = k.gettinup();
		tindown = k.gettindown();
		h = x[1]-x[0];
		valve = null;		
		dy_2 = new Double[n][n];//dy - ������� _2 - ��������� [������][�������]
		Lfp = new Double[n];
		R = new Double[n];				
		Lfp_b = new Double[n];
		R_b = new Double[n];		
		opts = n;//�����������
	}
	
	//���������� ����� (������) ���������� ����, ����� ��� ������ � ������ ��� �����, �� ����� ������������
	public void indexfortin(Double ti, String trend){		
		valve = calctrend(trend);		
		int n_ = 0;		
		n_=n-1;		
		index = null;
		int i=0;		
		Boolean key = null;		
		while(i<n_){			
			//������ ������� ������� �
			if (x[0]<x[x.length-1]){				
				//������
				if (valve) key = (x[i]<=ti && x[i+1]>ti);				
				//�����
				else { 										
					if (x[i]<ti && x[i+1]>ti) { 
						index = i+1; break;
						}													
				}
			}			
			
			else {	
				//������������ � ���������� ��� �����, ��� ������������� ������������ �������� �������
				/*				
				//������
				if (valve) key = (_x[i] >= ti && _x[i+1]<ti);				
				//�����
				else{ if (_x[i]>ti && _x[i+1]<ti) { index = i+1; break;} }
				*/				
			}			
			
			if (key!=null && key){
				index = i;
				break;
			}
			i++;
		}		
	}
	
	//������� ���������� ������� dy_2 ������ (y_2[all][0]=y[])
	public void dy_2_forward(){		
		int i = 0;
		while(i<n){
			dy_2[i][0] = y[i];
			i++;
		}	
		int n_ = n-1;
		i=0;
		int j = 1;
		while(j<n){
			while(i<n_){
				dy_2[i][j] = dy_2[i+1][j-1]-dy_2[i][j-1];
				i++;
			}
			i=0;
			n_--;
			j++;
		}		
	}
	
	//������� ���������� q
	public void calcq(Double ti, Double _x[]){
		h = _x[1]-_x[0];
		q = (ti - _x[index])/h;		
	}
	
	//���������� �������� dy_2m - ������� dy, s - �������;
	private Double calcL(Double dy_2m[][], int s){		
		print( " q = " + q );
		print( " index = " + index );
		Double summ;
		print("\n������� "+ s+" \n-----------------------------------");
		int ssu = 0; //��������� ������� ����		
		Double L = dy_2m[index][0];		
		print("	����� ����� " + ssu + " ����� " + L +"\n+\n");		
		Integer finalindex = index;		
		ssu = 1;
		int ssp = 1; //��������� ������� ������������ 
		Double pr = 1d;				
		while(ssu <= s){  			
			if (ssu>1) while( ssp < ssu ){  		
				if (valve) pr = pr * (q-ssp);
				else pr = pr * ( q + ssp );				
				print("		������������ ����� " + ssp + " ����o " + (pr * (q-ssp)));
				ssp++;
			}
			 if (!valve){
				finalindex--;//���� ������������ ����� ����� dy ������ ��������� �� ���������
				if (finalindex==-1) break;				
			 }
			summ = (  (q * pr) / (fact((double)ssu) ) * dy_2m[finalindex][ssu]);			
			pr = 1d;
			print("	����� ����� " + ssu + " ����� " + summ+"\n+\n");
			L += summ;
			ssp = 1;
			ssu++;
		}
		print("L = "+L+" \n-----------------------------------\n");
		return L;
	}
	
	//������� ������� �������� � ����� � ��� �� �����, �� � ������ ��������
	public void calcLfp(Double dy_2m[][], Double L[]){		
		int si =0; //stop index;		
		if (valve) si = firstindexnullvalue(dy_2m)-1;
		else si = index-1;				
		int s = 1;
		while(s<=si){
			L[s-1] = calcL(dy_2m, s);
			s++;
		}		
	}
	
	//�����. ������� ������� � null � ��...
	private Integer firstindexnullvalue(Double dy_2m[][]){
		int i = 1;		
		if (valve){
			if ( dy_2m[index][n-1] != null ) return n;
			else
				while(i<n){//���������� ����� ���������� ������� �� ���������						
					if ( dy_2m[index][i] == null ) return i;			
					i++;
				}
		}
		else{	return index+1;	}
		
		return null;		
	}
	
	//������� ���������� ���������� ����. ������� ����. - 170
	private Double fact(Double a){
		int i = 1;
		Double f = 1d;
		if (a<0) return null;
		if (a<=1) return 1d;
		else
		while(i<=a){
			f = f * i;			
			i++;
		}		
		return f;	
	}
	
	//������ �����������
	private Double calcR(int s, Double dy_2m[][]){
		int finalindex = index;
		Boolean en = null;
		int i=1;
		Double pr = 1d;
		Double R = null;	
		while (i<=s){
			if (valve) {	pr = pr * ( q - i );	}
			else{	pr = pr * ( q + i );	}
			i++;
		}
		if (!valve){
			finalindex -=s+1;//���� ������������ ����� ����� dy ������ ��������� �� ���������
			if (finalindex==-1) 
				return 888d;//����� ��� �������. (������������) 
			if (dy_2m[finalindex][s+1]!=null)
			en = true;
			else en = false;							
		 }
		else{
			if (dy_2m[finalindex][s+1]!=null) 
			en = true;
			else en = false;
		}		
		if (en!=null && en){ 
			R = Math.abs((dy_2m[finalindex][s+1]*q*pr)/fact((double)s+1));			
		}else{
			R = 999d;
		}		
		return R;
	}
	
	//������ ������������
	public void calcallR(Double dy_2m[][], Double _R[]){
		int i = 1;		
		int _n = 0;
		if (valve) _n = firstindexnullvalue(dy_2m)-1;
		else _n=index;
		while(i<_n){
			_R[i-1] = calcR(i, dy_2m);
			i++;
		}		
	}
	
	//������
	public Double[][] 	getdy_2()		{return dy_2;}	
	public Double[] 	getLfp()		{return Lfp;}
	public Double[] 	getR()			{return R;}	
	//�����
	public Double[] 	getLfp_b()		{return Lfp_b;}
	public Double[] 	getR_b()		{return R_b;}
	//��� ������� ����� ����� ��������������� �������������� �������� �������� optimals()
	public void setypogr(Double[] ypogr)	{this.ypogr = ypogr;}	
	
	//������������ �� ���� ������� ������ ��� ����� - trend, � �������� ����� - step
	public void calcLsteptrend(Double step, String trend){		
		Boolean forward = calctrend(trend);		
		Double dy[][];
		Double start_x;
		Double finish_x;		
		dy = dy_2;
		start_x = x[0];
		finish_x = x[n-1];
		//��������������� � ������������� ����������� ��������
		List<Double> X = new ArrayList<>(); //� �� ���������
		List<Double> L = new ArrayList<>();	//������� �� ���������	
		int i = 0;
		Integer clone;		
		//���������� ������� �
		if (start_x<finish_x){
			while(start_x < finish_x){
				X.add(start_x);									
				clone = findelement( X.get(i), x );//����, ���� �� ����������
				//������� ��� �������� � �������� �������
				if ( clone != null) L.add(dy[clone][0] );
				//������� ����������� � �������� �������
				else{
					indexfortin(X.get(i), trend);
					print( " ��������� ������ �� ����� " + index );
					possibles(dy);
					calcq(X.get(i), x);
					print( " �� = " + X.get(i) );
					print( " ������� ������ � " + x[index] );
					L.add(calcL(dy, possibles));
				}
				start_x += step;
				i++;						
			}
		}
		//�������� ������� � 
		else {
		//������������ � ���������� ��� ����� ���� ����� ������������ �������� �������
		/*
			while(start_x >= finish_x){
				X.add(start_x);
				indexfortin(X.get(i), trend);
				possibles(dy);				
				clone = findelement( X.get(i), x_ );//����, ���� �� ����������
				if ( clone != null) L.add ( dy[clone][0] );
				else{//���������� �� �����, ����� ����������� �������
					if (index!=0) index--;
					calcq(X.get(i), x_);
					print( " ti = " + X.get(i));
					print( "xr[index] = "+ x_[index]);
					L.add(calcL(dy, possibles));
				}						
				start_x -= step;
				i++;
			}
		*/				
		}		
		if (forward){			
			X_forward = X.toArray( new Double[X.size()] );
			L_forward = L.toArray( new Double[X.size()] );
		}
		else {
			X_ago = X.toArray( new Double[X.size()] );
			L_ago = L.toArray( new Double[X.size()] );
		}
	}	
	
	//����� ������ ������ ���������� null  � �������� ��� � opts � �������
	void possibles(Double D_2[][]){
		//����������� ����������� ��������� � ������
		if (valve) possibles = firstindexnullvalue(D_2)-1;
		else possibles = index;
		//����������� ����������� ��������
		if ( possibles > opts) possibles = opts;
	}	
	
	//����� ������� � ������������
	public void writetofile(String trend, PrintWriter file){
		calctrend(trend);		
		String _s[] = {"x","y","dy^..","dy^..","dy^..","dy^..","dy^..","dy^..","dy^..","dy^..","dy^.."};
		Double ti = null;
		Double dy[][] = null;
		Double _x[] = null;
		Double _Lfp[] = null;
		Double _R[] = null;		
			if (valve){
				trend = "������� 2.1(2) - ������";
				ti = tinup;
				dy = dy_2;
				_x = x;
				_Lfp = Lfp;
				_R = R;
				indexfortin(ti, "������");//���. ����. ����� � ����� ������������
				int indexfirstnull = firstindexnullvalue(dy);
				//���� ������ ������� �������� ������� = 10
				if (indexfirstnull == 10) {String s[] = {"x","y","dy","dy^2","dy^3","dy^4","dy^5","dy^6","dy^7","dy^8","dy^9"};_s=s;}
				//����� ���� ������ ������� �������� ������� = 9  
				else if (indexfirstnull == 9) {String s[] = {"x","y","dy","dy^2","dy^3","dy^4","dy^5","dy^6","dy^7","dy^8"};_s=s;}
				//����� ���� ������ ������� �������� ������� = 8  
				else if (indexfirstnull == 8) {String s[] = {"x","y","dy","dy^2","dy^3","dy^4","dy^5","dy^6","dy^7"};_s=s;}				
			}			
			else{
				trend = "\n\n������� 2.3 - �����";
				ti = tindown;
				dy = dy_2;
				_x = x;
				_Lfp = Lfp_b;
				_R = R_b;
			}			
			//����� ������� 2.x			
			file.println(trend);//����� �������� �������
			if (valve) {
				//����� ��������� (x y dy...) ������� 2.x
				writemasstringtab(file,_s);
				file.print("\n����� ������������ - "+tinup);
			}
			else file.println("����� ������������ - "+tindown);
			//����� ������ ������� 2.1	
				//����� - ������� ������� � �
				String buf[] = new String[1];
				int i = 0;
				if (valve) while(i<n){
					if (_x[i] == null) buf[0] = "null";
					else buf[0] = "\n" + DelPoint.dp(Double.toString(_x[i]));
					writemasstringtab(file, buf);
					//����� ������ ������� dy
					int j = 0;
					while(j<n){
						if (dy[i][j] == null) buf[0] = "null";
						else buf[0] = DelPoint.dp(Double.toString(dy[i][j]));
						writemasstringtab(file, buf);
						j++;
					}
					j=0;
					i++;
				}
				i = 1;				
				//����� ������ ���������
				_s = new String[n];
				if (valve) _s[0] = "\n��������	";
				else _s[0] = "��������	";
				while (i<n){
					if (_Lfp[i-1] == null) _s[i] = "null"; 
					else _s[i] = DelPoint.dp(Double.toString(_Lfp[i-1]));					
					i++;
				}
				i=1;
				writemasstringtab(file, _s);				
				//����� ������ ������������
				_s[0] = "\n�����������	";
				while (i<n){
					if (_R[i-1] == null) _s[i] = "null"; 
					else _s[i] = DelPoint.dp(Double.toString(_R[i-1]));
					i++;
				}
				i=1;
				writemasstringtab(file, _s);						
	}			
	
	//����� ��������� �� ���������
	public void writetofile(PrintWriter file, String trend){
		Double _x[];
		Double _L[];
		if (calctrend(trend)){
			_x = X_forward;
			_L = L_forward;
			trend = "\n\n������� ��������� ������ - index, x, y";
		}
		else {
			_x = X_ago;
			_L = L_ago;
			trend = "\n������� ��������� ����� - index, x, y";
		}	
		//��������� �������
		file.println(trend);
		writefile(file, 0, _x.length);//index
		writefile(file, _x, 0, _x.length);//x
		writefile(file, _L, 0, _x.length);//y
				
	}	
	
	private Boolean calctrend(String trend){
		Boolean forward = null;
		if (trend == "������") forward = true; 
		if (trend == "�����") forward = false;
		valve = forward;
		return forward;
	}
	
}