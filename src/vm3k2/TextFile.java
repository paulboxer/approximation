package vm3k2;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

//������ � ����� � ����
public class TextFile extends DelPoint{	
	public PrintWriter outlagrang;
	public PrintWriter outnewton;
	public PrintWriter outbox;
	public PrintWriter debuglog;
	public BufferedReader _in;
	public boolean filedownload = false;
	
	public TextFile(){super();}
	public TextFile(String PathOut){ 
		 try {
			 outlagrang = new PrintWriter(new File(PathOut+"\\outlagrang.xls").getAbsoluteFile());
			 outnewton = new PrintWriter(new File(PathOut+"\\outnewton.xls").getAbsoluteFile());
			 outbox = new PrintWriter(new File(PathOut+"\\outbox.xls").getAbsoluteFile());
			 debuglog = new PrintWriter(new File(PathOut+"\\debuglog.txt").getAbsoluteFile());
		 } catch(IOException e) {throw new RuntimeException(e);}
	}
	
	
	public void close(){
		outlagrang.close();
		outnewton.close();
		outbox.close();
		debuglog.close();
		System.out.println("��� ����� ��������� �������");
	}	
	
  //��������� ���� � �������� � ���� ������ String (�������� �� ����� ��������������)
  public ArrayList<String> read(String fileName) {
      ArrayList<String>ALS = new ArrayList<String>();
	  //StringBuilder sb = new StringBuilder();
      try {
          BufferedReader in = new BufferedReader(new FileReader( new File(fileName).getAbsoluteFile()));
          try {
              String s;
              while ((s = in.readLine()) != null) {
                  ALS.add(s+"\n");
            	  //sb.append(s);
                  //sb.append("\n");
              }
          } finally {
              in.close();
          }
      } catch(IOException e) {
          throw new RuntimeException(e);
      }
      return ALS;
  }
  
  //������������ ���������� �� �����
  public String readonesymbol(String fileName){
	  String s = "";	  
	  try {
          if (!filedownload){
        	  _in = new BufferedReader(new FileReader( new File(fileName).getAbsoluteFile()));
        	  filedownload = true;
          }
          try {              
             s = _in.readLine();                  
          } finally {
              if (s == null) { 
            	  _in.close(); 
            	  filedownload = false; 
            	  System.out.println("������� ���� �������� � ������, ����� �������");
              }
          }
      } catch(IOException e) {
          throw new RuntimeException(e);
      }
      return s;
  }  
  
  //���� ������� ������������ � ����----------------------------------------------------
  //����� � ���� ����������� ����������� ������� (�������� �� ����� ��������������)
  public static void writeln(String s, PrintWriter pw){
	   for (int i=0; i<s.length(); i++){
		   pw.println(s.charAt(i));		  
	  }
  }  
 
 public void writefile(PrintWriter file, Double[] d, int startvalue, int endvalue){
		while(startvalue<endvalue){
			//���������, �������, ������ ����� �� �������
			String s = DelPoint.dp(new BigDecimal(d[startvalue]).setScale(6, RoundingMode.HALF_UP).doubleValue()+"	");
			file.print(s);
			startvalue++;
		}
		file.println();
	}
	
  //��������� �������� �������
  public void writefile(PrintWriter file, int startvalue, int endvalue){ 
		while(startvalue<endvalue){
		file.print(startvalue+++"	");
		}
		file.println();
	}
	
	//����� � ��������� ���������, ��� ����� �������� ������ ������ � �������
	public void writefile(PrintWriter file, Double[] d, int startvalue, int endvalue, int beginshift){
		while(beginshift<startvalue){
			beginshift++;
			file.print("	");
		}
		writefile(file, d, startvalue, endvalue);
	}
	
	//������ ���������� �������, ������ ������� ����� ���
	public void writemasstringtab(PrintWriter file, String[] s){
		for (int i=0; i<s.length; i++){			   
			file.print(s[i]+"	");		  
		}
	}	
	
	//���������� ������ ������ � �������� ������� ������
	public static String[] GetListFileInInputFolder(){
		String path;
		path = System.getProperty("user.dir");
		path += "\\InFolder";
		File f = new File(path);
		String[] list = f.list();
		return list;		
	}
	
	//���������� ������ ��������� � �������� ������� ������
		public static String[] GetListFoldInOutputFolder(){
			String path;
			path = System.getProperty("user.dir");
			path += "\\OutFolder";
			File f = new File(path);
			String[] list = f.list();
			return list;		
		}	
	
	public String createFolder(String NameFolder){
		NameFolder = System.getProperty("user.dir") + "\\OutFolder\\" + NameFolder;
		if (!new File(NameFolder).exists()) {
		    if (!new File(NameFolder).mkdirs()) {
		        System.out.println("Creating '" + NameFolder + "' directory has failed");
		    }
		}
		return NameFolder;
	}		
	
	/*
	public static void main(String[] args) {
       System.out.println("Testing list file in folder");
	   String[] list = GetListFileInInputFolder(); 
       for (String item: list){
			System.out.println(item);
		}
     */	
	
 }
